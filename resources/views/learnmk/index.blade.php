@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <strong>{{ Session::get('success') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <strong>{{ Session::get('error') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            <h4 class="card-title">
                                Cap nhat điểm học lại
                            </h4>
                            <form id="form-create" action="{{ url('/learnmk') }}" method="POST" style="overflow-y: auto"
                                enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $learnag->id }}">
                                <div>
                                    <div>
                                        <div>
                                            <label for="title-create" class="col-form-label">
                                                <b>Tên: <span style="color: red">*</span></b>
                                            </label>
                                            <select class="form-control form-control-lg" id="exampleFormControlSelect2"
                                                name="student_id" readonly>
                                                <option selected  value="{{ $learnag->student->id }}">{{ $learnag->student->name }}</option>
                                            </select>
                                        </div>
                                        <div>
                                            <label for="title-create" class="col-form-label">
                                                <b>Môn: <span style="color: red">*</span></b>
                                            </label>
                                            <select class="form-control form-control-lg" id="exampleFormControlSelect2"
                                                name="subject_id" readonly>
                                                <option selected value="{{ $learnag->subjects->id }}">{{ $learnag->subjects->name }}</option>
                                            </select>
                                        </div>
                                        <div>
                                            <label for="title-create" class="col-form-label">
                                                <b>Điểm thành phần: <span style="color: red">*</span></b>
                                            </label>
                                            <input type="text" class="form-control" name="point_ingredient"
                                                placeholder="Thêm điểm" required>
                                        </div>
                                        <div>
                                            <label for="title-create" class="col-form-label">
                                                <b>Điều kiện: <span style="color: red">*</span></b>
                                            </label>
                                            <select class="form-control form-control-lg" id="exampleFormControlSelect2"
                                                name="status">
                                                <option disabled>Chọn điều kiện</option>
                                                <option value="1">Đã đóng tiền</option>
                                                <option value="0">Chưa đóng tiền</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" onclick="history.back()">Thoát</button>
                                    <div>
                                        <button type="submit" class="btn btn-info">Chấp nhận</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
