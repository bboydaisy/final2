@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <strong>{{ Session::get('success') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <strong>{{ Session::get('error') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            <h4 class="card-title">Giáo Vụ</h4>
                            <a class="btn btn-info" href="{{ url('teacher-create') }}">
                                <i class="icon-upload"></i>
                                <code>Thêm giáo vụ</code>
                            </a>
                            <div class="table-responsive pt-3">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Email</th>
                                            <th>Cấp Độ</th>
                                            <th>Số Điện Thoại</th>
                                            <th>Sửa</th>
                                            <th>Xoá</th>
                                        </tr>
                                        @foreach ($teachers as $teacher)
                                            <tr>
                                                <td>
                                                    {{ $teacher->name }}
                                                </td>
                                                <td>
                                                    {{ $teacher->email }}
                                                </td>
                                                <td>
                                                    {{ $teacher->capdo }}
                                                </td>
                                                <td>
                                                    {{ $teacher->phone }}
                                                </td>
                                                <td>
                                                    <a class="btn btn-warning"
                                                        href="{{ route('teacher-edit', $teacher->id) }}">
                                                        <i class="mdi mdi-file-check btn-icon-append"></i>
                                                        Sửa
                                                    </a>
                                                </td>
                                                <td>
                                                    @if ($teacher->confirm_flag == 0)
                                                        <form action="{{ url('/teacher') }}" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="hidden" name="id" value="{{ $teacher->id }}">
                                                            <button class="btn btn-danger btn-icon-text"
                                                                onclick="return confirm('Bạn có muốn xóa tài khoản này không?')">
                                                                <i class="mdi mdi-alert btn-icon-prepend"></i>
                                                                Xóa
                                                            </button>
                                                        </form>
                                                    @else
                                                        <button class="btn btn-block btn-icon-text" disabled>
                                                            <i class="mdi mdi-alert btn-icon-prepend"></i>
                                                            Xóa
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination">
                {{ $teachers->links() }}
            </div>
        </div>
    </div>
@endsection
