@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <strong>{{ Session::get('success') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <strong>{{ Session::get('error') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            <h4 class="card-title">Khóa Học</h4>
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalCreate">
                                <i class="icon-upload"></i>
                                <code class="text-white">Thêm khóa học</code>
                            </button>
                            <div class="table-responsive pt-3">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Sửa</th>
                                            {{-- <th>Xóa</th> --}}
                                        </tr>
                                        @foreach ($courses as $course)
                                            <tr>
                                                <td>
                                                    {{ $course->name }}
                                                </td>
                                                <td>
                                                    <a class="btn btn-warning"
                                                        href="{{ route('course-edit', $course->id) }}">
                                                        <i class="mdi mdi-file-check btn-icon-append"></i>
                                                        Sửa
                                                    </a>
                                                </td>
                                                {{-- <td>
                                                    <form action="{{ route('course.destroy', $course) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger btn-icon-text"
                                                            onclick="return confirm('Tất cả sản phẩm liên quan đến danh mục này sẽ bị xóa. bạn có chắc muốn xóa danh mục không?')">
                                                            <i class="mdi mdi-alert btn-icon-prepend"></i>
                                                            Xóa
                                                        </button>
                                                    </form>
                                                </td> --}}
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination">
                {{ $courses->links() }}
            </div>
        </div>
    </div>

    {{-- modalCreate --}}
    <div class="modal fade" id="modalCreate" tabindex="-1" aria-labelledby="CreatePostModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-create" action="{{ url('/course') }}" method="POST" style="overflow-y: auto"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h4>Mange</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Tên khóa học: <span style="color: red">*</span></b>
                                </label>
                                <input type="text" class="form-control" name="name" placeholder="Thêm tên..." required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                        <div>
                            <button type="submit" class="btn btn-info">Chấp nhận</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
