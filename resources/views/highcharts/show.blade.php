@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Môn Học ( Lớp {{$classrm->name}} )</h4>
                            <div class="table-responsive pt-3">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Hành Động</th>
                                        </tr>
                                        @foreach ($subjectss as $subjects)
                                            <tr>
                                                <td>
                                                    {{ $subjects->name }}
                                                </td>
                                                <td>
                                                    {{-- <a class="btn btn-info"
                                                        href="{{ route('chart-show2', $subjects->id) }}">
                                                        Xem
                                                    </a> --}}
                                                    <form action="{{route('statistical')}}" method="GET">
                                                        @csrf
                                                        <input type="hidden" name="classrm" value="{{$classrm->id}}">
                                                        <input type="hidden" name="subjects" value="{{$subjects->id}}">
                                                        <button class="btn btn-info">Xem</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
