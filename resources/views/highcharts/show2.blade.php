@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Bảng điểm ({{ $classrm->name }}) ({{ $subjects->name }})</h4>`
                            {{-- <a class="btn btn-success" hidden href="{{ route('file-export-mark') }}">Export Excel</a> --}}
                            <div class="table-responsive pt-3">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Điểm Điều Kiện</th>
                                            <th>Điểm Lần 1</th>
                                            <th>Điểm Lần 2</th>
                                            <th>Điểm Học Lại Lần 1</th>
                                            <th>Điểm Học Lại Lần 2</th>
                                        </tr>
                                        @foreach ($conditions as $condition)
                                            <tr>
                                                <td>
                                                    {{ $condition->student->name }}
                                                </td>
                                                <td>
                                                    {{ $condition->point_ingredient }}
                                                </td>
                                                <td>
                                                    {{ $condition->mark1 }}
                                                </td>
                                                <td>
                                                    {{ $condition->mark2 }}
                                                </td>
                                                @foreach ($learnags as $learnag)
                                                    @if ($condition->student_id == $learnag->student_id)
                                                    <td>
                                                        {{ $learnag->mark1 }}
                                                    </td>
                                                    <td>
                                                        {{ $learnag->mark2 }}
                                                    </td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="pagination">
                {{ $mark1s->links() }}
            </div> --}}
        </div>
    </div>
@endsection
