@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Default form</h4>
                            <p class="card-description">
                                Sửa bảng điểm
                            </p>
                            <form class="forms-sample" action="{{ url('/markag1') }}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="id" value="{{ $mark1->id }}">
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Tên sinh viên:</label>
                                    <input type="text" class="form-control" id="exampleInputUsername1"
                                        value="{{ $mark1->student->name }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Tên môn học:</label>
                                    <input type="text" class="form-control" id="exampleInputUsername1"
                                        value="{{ $mark1->subjects->name }}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Điều kiện:</label>
                                    <select class="form-control form-control-lg" id="exampleFormControlSelect2"
                                        name="status">
                                        <option disabled selected>
                                            Chọn điều kiện
                                        </option>
                                        <option value="1" @if ($mark1->status == 1) {{ 'selected' }} @endif>
                                            Đã đóng tiền
                                        </option>
                                        <option value="0" @if ($mark1->status == 0) {{ 'selected' }} @endif>
                                            Chưa đóng tiền
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Điểm:</label>
                                    <input type="text" name="mark1" class="form-control" id="exampleInputUsername1k1"
                                        placeholder="Sửa điểm" value="{{ $mark1->mark1 }}" required>
                                </div>
                                <button class="btn btn-info btn-icon-text" type="submit">
                                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                                    Chấp Nhận
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
