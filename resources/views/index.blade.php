@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-sm-12 mb-4 mb-xl-0">
                    <h4 class="font-weight-bold text-dark">Hi, welcome back!</h4>
                    <p class="font-weight-normal mb-2 text-muted">{{ $date->format('l jS \\of F Y') }}</p>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-xl-3 flex-column d-flex grid-margin stretch-card">
                    <div class="row flex-grow">
                        <div class="col-sm-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Khóa Học</h4>
                                    <p>tổng số khóa học</p>
                                    <h4 class="text-dark font-weight-bold mb-2">{{ $courses }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Lớp Học</h4>
                                    <p>tổng số lớp học</p>
                                    <h4 class="text-dark font-weight-bold mb-2">{{ $classrms }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 flex-column d-flex grid-margin stretch-card">
                    <div class="row flex-grow">
                        <div class="col-sm-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Ngành Học</h4>
                                    <p>tổng số ngành học</p>
                                    <h4 class="text-dark font-weight-bold mb-2">{{ $majorss }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Môn Học</h4>
                                    <p>tổng số môn học</p>
                                    <h4 class="text-dark font-weight-bold mb-2">{{ $subjectss }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 flex-column d-flex grid-margin stretch-card">
                    <div class="row flex-grow">
                        <div class="col-sm-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Sinh Viên</h4>
                                    <p>tổng số sinh viên</p>
                                    <h4 class="text-dark font-weight-bold mb-2">{{ $students }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Giáo vụ</h4>
                                    <p>tổng số giáo vụ</p>
                                    <h4 class="text-dark font-weight-bold mb-2">{{ $teachers }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 flex-column d-flex grid-margin stretch-card">
                    <div class="row flex-grow">
                        <div class="col-sm-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Thi Lại</h4>
                                    <p>tổng số sinh viên thi lại</p>
                                    <h4 class="text-dark font-weight-bold mb-2">{{ $total_exam }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Học Lại</h4>
                                    <p>tổng số sinh viên học lại</p>
                                    <h4 class="text-dark font-weight-bold mb-2">{{ $total_learn }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
