@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Default form</h4>
                            <p class="card-description">
                                Sửa Lớp Học
                            </p>
                            <form class="forms-sample" action="{{ url('/class') }}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="id" value="{{ $classrm->id }}">
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Tên khóa học:</label>
                                    <select class="form-control form-control-lg" id="exampleFormControlSelect2"
                                        name="course_id">
                                        @foreach ($courses as $course)
                                            <option value="{{ $course->id }}"
                                                @if ($course->id == $classrm->course_id) {{ 'selected' }} @endif>
                                                {{ $course->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Tên lớp học:</label>
                                    <input type="text" name="name" class="form-control" id="exampleInputUsername1"
                                        placeholder="Sửa tên" value="{{ $classrm->name }}" required>
                                </div>
                                <button class="btn btn-info btn-icon-text" type="submit">
                                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                                    Chấp Nhận
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
