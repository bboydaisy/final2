@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <strong>{{ Session::get('success') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <strong>{{ Session::get('error') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            <h4 class="card-title">Bảng điểm ({{ $classrm->name }})</h4>
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalCreate">
                                <i class="icon-upload"></i>
                                <code class="text-white">Thêm Điểm</code>
                            </button>
                            <a class="btn btn-success" hidden href="{{ route('file-export-conditon') }}">Export Excel</a>
                            <div class="table-responsive pt-3">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Điểm thành phần</th>
                                            <th>Môn</th>
                                            <th>Trạng Thái</th>
                                            <th>Ngày</th>
                                        </tr>
                                        @foreach ($conditions as $condition)
                                            <tr>
                                                <td>
                                                    {{ $condition->student->name }}
                                                </td>
                                                <td>
                                                    {{ $condition->point_ingredient }}
                                                </td>
                                                <td>
                                                    {{ $condition->subjects->name }}
                                                </td>
                                                <td>
                                                    @if ($condition->status == 1 && $condition->point_ingredient >= 5)
                                                        @if ($condition->mark1 == null && $condition->mark2 == null)
                                                            <span style="color: green; font-weight: bold">
                                                                Đủ điều kiện
                                                            </span>
                                                        @elseif ($condition->mark1 >= 5 || $condition->mark2 >= 5)
                                                            <span style="color: green; font-weight: bold">
                                                                Qua môn
                                                            </span>
                                                        @elseif ($condition->mark2 != null && $condition->mark2 < 5)
                                                            <span style="color: red; font-weight: bold">
                                                                Học lại
                                                            </span>
                                                        @elseif ($condition->mark1 != null && $condition->mark1 < 5)
                                                            <span style="color: red; font-weight: bold">
                                                                Thi lại
                                                            </span>
                                                        @endif
                                                    @else
                                                        @if ($condition->mark1 >= 5 || $condition->mark2 >= 5)
                                                            <span style="color: green; font-weight: bold">
                                                                Qua môn
                                                            </span>
                                                        @elseif ($condition->mark2 != null && $condition->mark2 < 5)
                                                            <span style="color: red; font-weight: bold">
                                                                Học lại
                                                            </span>
                                                        @elseif ($condition->mark1 != null && $condition->mark1 < 5)
                                                            <span style="color: red; font-weight: bold">
                                                                Thi lại
                                                            </span>
                                                        @else
                                                            <span style="color: red; font-weight: bold">
                                                                Thi lại
                                                            </span>
                                                        @endif
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ date('d-m-Y H:i', strtotime($condition->created_at)) }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination">
                {{ $conditions->links() }}
            </div>
        </div>
    </div>

    {{-- modalCreate --}}
    <div class="modal fade" id="modalCreate" tabindex="-1" aria-labelledby="CreatePostModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-create" action="{{ url('/condition') }}" method="POST" style="overflow-y: auto"
                    enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{ $classrm->id }}">
                    <div class="modal-header">
                        <h4>Manage</h4>
                    </div>
                    @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <strong>{{ Session::get('success') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <strong>{{ Session::get('error') }}</strong>
                                    <a href="" class="close" data-dismiss="alert"
                                        aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <a href="" class="close" data-dismiss="alert"
                                        aria-label="close">&times;</a>
                                </div>
                            @endif
                    <div class="modal-body">
                        <div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Tên: <span style="color: red">*</span></b>
                                </label>
                                <select class="form-control form-control-lg" id="exampleFormControlSelect2"
                                    name="student_id">
                                    <option disabled selected>Chọn sinh viên</option>
                                    @foreach ($students as $student)
                                        <option value="{{ $student->id }}">{{ $student->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Môn: <span style="color: red">*</span></b>
                                </label>
                                <select class="form-control form-control-lg" id="exampleFormControlSelect2"
                                    name="subject_id">
                                    <option disabled selected>Chọn môn học</option>
                                    @foreach ($subjectss as $subjects)
                                        <option value="{{ $subjects->id }}">{{ $subjects->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Điểm thành phần: <span style="color: red">*</span></b>
                                </label>
                                <input type="text" class="form-control" name="point_ingredient" placeholder="Thêm điểm"
                                    required>
                            </div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Điều kiện: <span style="color: red">*</span></b>
                                </label>
                                <select class="form-control form-control-lg" id="exampleFormControlSelect2" name="status">
                                    <option disabled selected>Chọn điều kiện</option>
                                    <option value="1">Đã đóng tiền</option>
                                    <option value="0">Chưa đóng tiền</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                        <div>
                            <button type="submit" class="btn btn-info">Chấp nhận</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
