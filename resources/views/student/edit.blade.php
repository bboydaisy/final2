@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Default form</h4>
                            <p class="card-description">
                                Sửa Sinh Viên
                            </p>
                            <form class="forms-sample" action="{{ url('/student') }}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="id" value="{{ $student->id }}">
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Tên sinh viên:</label>
                                    <input type="text" name="name" class="form-control" id="exampleInputUsername1"
                                        placeholder="Sửa tên" value="{{ $student->name }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Tên lớp học:</label>
                                    <select class="form-control form-control-lg" id="exampleFormControlSelect2"
                                        name="class_id">
                                        @foreach ($classrms as $classrm)
                                            <option value="{{ $classrm->id }}"
                                                @if ($classrm->id == $student->class_id) {{ 'selected' }} @endif>
                                                {{ $classrm->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Giới tính:</label>
                                    <select class="form-control form-control-lg" id="exampleFormControlSelect2" name="sex">
                                        <option value="1" @if ($student->sex == 1) {{ 'selected' }} @endif>
                                            Nam
                                        </option>
                                        <option value="0" @if ($student->sex == 0) {{ 'selected' }} @endif>
                                            Nữ
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputUsername1">Số điện thoại:</label>
                                    <input type="text" name="phone" class="form-control" id="exampleInputUsername1"
                                        placeholder="Sửa số điện thoại" value="{{ $student->phone }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="title-create" class="col-form-label">Địa chỉ:</label>
                                    <textarea class="form-control" style="height:100px" placeholder="Thêm địa chỉ..."
                                        name="address" id="content-create" required>{{ $student->address }}</textarea>
                                </div>
                                <button class="btn btn-info btn-icon-text" type="submit">
                                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                                    Chấp Nhận
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
