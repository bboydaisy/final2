@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <strong>{{ Session::get('success') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <strong>{{ Session::get('error') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            <h4 class="card-title">Sinh Viên</h4>
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalCreate">
                                <i class="icon-upload"></i>
                                <code class="text-white">Thêm sinh viên</code>
                            </button>
                            <a class="btn btn-success" href="{{ route('file-export') }}">Export Excel</a>
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                data-bs-target="#exampleModal">
                                Import Excel
                            </button>
                            <div class="table-responsive pt-3">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Mã</th>
                                            <th>Tên</th>
                                            <th>Lớp Học</th>
                                            <th>Khóa Học</th>
                                            <th>Giới Tính</th>
                                            <th>Số Điện Thoại</th>
                                            <th>Địa Chỉ</th>
                                            <th>Sửa</th>
                                        </tr>
                                        @foreach ($students as $student)
                                            <tr>
                                                <td>
                                                    {{ $student->id }}
                                                </td>
                                                <td>
                                                    {{ $student->name }}
                                                </td>
                                                <td>
                                                    {{ $student->classrm->name }}
                                                </td>
                                                <td>
                                                    {{ $student->classrm->course->name }}
                                                </td>
                                                <td>
                                                    {{ $student->gioitinh }}
                                                </td>
                                                <td>
                                                    {{ $student->phone }}
                                                </td>
                                                <td>
                                                    {{ $student->address }}
                                                </td>
                                                <td>
                                                    <a class="btn btn-warning"
                                                        href="{{ route('student-edit', $student->id) }}">
                                                        <i class="mdi mdi-file-check btn-icon-append"></i>
                                                        Sửa
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination">
                {{ $students->links() }}
            </div>
        </div>
    </div>

    {{-- modalCreate --}}
    <div class="modal fade" id="modalCreate" tabindex="-1" aria-labelledby="CreatePostModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="form-create" action="{{ url('/student') }}" method="POST" style="overflow-y: auto"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h4>Manage</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Tên sinh viên: <span style="color: red">*</span></b>
                                </label>
                                <input type="text" class="form-control" name="name" placeholder="Thêm tên..." required>
                            </div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Tên lớp học: <span style="color: red">*</span></b>
                                </label>
                                <select class="form-control form-control-lg" id="exampleFormControlSelect2" name="class_id">
                                    <option disabled selected>Chọn lớp học</option>
                                    @foreach ($classrms as $classrm)
                                        <option value="{{ $classrm->id }}">{{ $classrm->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Giới tính: <span style="color: red">*</span></b>
                                </label>
                                <select class="form-control form-control-lg" id="exampleFormControlSelect2" name="sex">
                                    <option disabled selected>Chọn giới tính</option>
                                    <option value="1">Nam</option>
                                    <option value="0">Nữ</option>
                                </select>
                            </div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Số điện thoại: <span style="color: red">*</span></b>
                                </label>
                                <input type="text" class="form-control" name="phone" placeholder="Thêm số..." required>
                            </div>
                            <div>
                                <label for="title-create" class="col-form-label">
                                    <b>Địa chỉ:<span style="color: red">*</span></b>
                                </label>
                                <textarea class="form-control" style="height:100px" placeholder="Thêm địa chỉ..."
                                    name="address" id="content-create" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                        <div>
                            <button type="submit" class="btn btn-info">Chấp nhận</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modalimport --}}
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('file-import') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group mb-4" style="max-width: 500px; margin: 0 auto;">
                            <div class="custom-file text-left">
                                <input type="file" name="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary">Import data</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
