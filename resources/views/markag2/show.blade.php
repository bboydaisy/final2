@extends('layout.main')

@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <strong>{{ Session::get('success') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if (Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <strong>{{ Session::get('error') }}</strong>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                    <a href="" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                </div>
                            @endif
                            <h4 class="card-title">Bảng điểm ({{ $classrm->name }})</h4>
                            <a class="btn btn-success" hidden href="{{ route('file-export-mark2') }}">Export Excel</a>
                            <div class="table-responsive pt-3">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Điểm lần 1</th>
                                            <th>Điểm lần 2</th>
                                            <th>Môn</th>
                                            <th>Điều Kiện</th>
                                            <th>Trạng Thái</th>
                                            <th>Ngày</th>
                                            <th>Cập nhật</th>
                                            <th>Sửa</th>
                                        </tr>
                                        @foreach ($mark2s as $mark2)
                                            <tr>
                                                <td>
                                                    {{ $mark2->student->name }}
                                                </td>
                                                <td>
                                                    {{ $mark2->mark1 }}
                                                </td>
                                                <td>
                                                    @if ($mark2->mark2 == null)
                                                        <span style="color: gray; font-weight: bold">
                                                            Đang cập nhật
                                                        </span>
                                                    @else
                                                        {{ $mark2->mark2 }}
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $mark2->subjects->name }}
                                                </td>
                                                <td>
                                                    @if ($mark2->status == 1)
                                                        <span style="color: green; font-weight: bold">
                                                            Đã đóng tiền
                                                        </span>
                                                    @elseif ($mark2->status == 0)
                                                        <span style="color: red; font-weight: bold">
                                                            Chưa đóng tiền
                                                        </span>
                                                    @else
                                                        <span style="color: gray; font-weight: bold">
                                                            Đang cập nhật
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($mark2->mark2 !== null)
                                                        @if ($mark2->status == 1 && $mark2->mark2 >= 5)
                                                            <span style="color: green; font-weight: bold">
                                                                Qua môn
                                                            </span>
                                                        @else
                                                            <span style="color: red; font-weight: bold">
                                                                Học lại
                                                            </span>
                                                        @endif
                                                    @elseif ($mark2->mark1 < 5)
                                                        <span style="color: red; font-weight: bold">
                                                            Thi lại
                                                        </span>
                                                    @else
                                                        <span style="color: gray; font-weight: bold">
                                                            Đang cập nhật
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ date('d-m-Y H:i', strtotime($mark2->created_at)) }}
                                                </td>
                                                <td>
                                                    {{ date('d-m-Y H:i', strtotime($mark2->updated_at)) }}
                                                </td>
                                                <td>
                                                    <a class="btn btn-warning"
                                                        href="{{ route('markag2-edit', $mark2->id) }}">
                                                        <i class="mdi mdi-file-check btn-icon-append"></i>
                                                        Sửa
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination">
                {{ $mark2s->links() }}
            </div>
        </div>
    </div>
@endsection
