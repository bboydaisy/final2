<?php

namespace App\Exports;

use App\Models\Condition;
use Maatwebsite\Excel\Concerns\FromCollection;

class ConditionExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Condition::join('students', 'conditions.student_id', 'students.id')
            ->join('subjects', 'conditions.subject_id', 'subjects.id')
            ->select(
                'students.name',
                'subjects.name as name_subjects',
                'conditions.point_ingredient',
                'conditions.point_diligence',
                'conditions.point_gpa'
            )->get();
    }
}
