<?php

namespace App\Exports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Student::join('classrms', 'students.class_id', 'classrms.id')
        ->select('students.name', 'classrms.name as name_class', 'students.sex', 'students.phone', 'students.address')
        ->get();
    }
}
