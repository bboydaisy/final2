<?php

namespace App\Exports;

use App\Models\Mark2;
use Maatwebsite\Excel\Concerns\FromCollection;

class MarkExport2 implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Mark2::join('students', 'mark2s.student_id', 'students.id')
            ->join('subjects', 'mark2s.subject_id', 'subjects.id')
            ->select( 'students.name', 'subjects.name as name_subject','mark2s.point')->get();
    }
}
