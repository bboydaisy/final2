<?php

namespace App\Exports;

use App\Models\Mark1;
use Maatwebsite\Excel\Concerns\FromCollection;

class MarkExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Mark1::join('students', 'mark1s.student_id', 'students.id')
            ->join('subjects', 'mark1s.subject_id', 'subjects.id')
            ->select( 'students.name', 'subjects.name as name_subject','mark1s.point')->get();
    }
}
