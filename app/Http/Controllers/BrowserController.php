<?php

namespace App\Http\Controllers;

use App\Models\Classrm;
use App\Models\Condition;
use App\Models\Learnag;
use App\Models\Subjects;
use Illuminate\Http\Request;

class BrowserController extends Controller
{
    public function index()
    {
        $classrms = Classrm::with('course')->orderBy('course_id', 'desc')->paginate(20)->withQueryString();
        return view('highcharts.index', compact('classrms'));
    }

    public function show($id)
    {
        $classrm = Classrm::find($id);
        $subjectss = Subjects::orderBy('name', 'ASC')->get();
        return view('highcharts.show', compact('classrm','subjectss'));
    }

    public function statistical(Request $request)
    {
        $classrm = Classrm::where('id', $request->classrm)->first();
        $subjects = Subjects::where('id', $request->subjects)->first();
        $conditions = Condition::where('class_id', $request->classrm)->where('subject_id', $request->subjects)->get();
        $learnags = Learnag::where('class_id', $request->classrm)->where('subject_id', $request->subjects)->get();
        return view('highcharts.show2', compact('conditions','classrm','subjects', 'learnags'));
    }
}
