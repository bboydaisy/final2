<?php

namespace App\Http\Controllers;

use App\Models\Classrm;
use App\Models\Learnag;
use App\Models\Mark2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Exports\MarkExport2;
use App\Models\Condition;
use App\Models\Learnmk;
use Maatwebsite\Excel\Facades\Excel;

class MarkAgController2 extends Controller
{
    public function index()
    {
        $classrms = Classrm::with('course', 'condition')->orderBy('course_id', 'desc')->paginate(20)->withQueryString();
        $classrms->getCollection()->transform(function ($classrm) {
            $classrm->resit_count = $classrm->condition->whereNull('mark2')->whereNotNull('mark1')->where('mark1', '<', '5')->count();
            return $classrm;
        });
        return view('markag2.index', compact('classrms'));
    }


    public function show($id)
    {
        $classrm = Classrm::find($id);
        $mark2s = Learnag::with('student', 'subjects')->where([['class_id', $id], ['mark1', '<', '5']])->orderBy('id', 'desc')->paginate(20)->withQueryString();
        return view('markag2.show', compact('mark2s', 'classrm'));
    }


    public function edit($id)
    {
        $mark2 = Learnag::with('student', 'subjects')->find($id);
        return view('markag2.edit', compact('mark2'));
    }


    public function update(Request $request)
    {
        $mark2 = Learnag::find($request->id);
        $mark2->status = $request->status;
        $mark2->mark2 = $request->mark2;
        $mark2->save();
        return Redirect::route('markag2-show', $mark2->class_id)->with('success', 'Cập nhật thành công!');
    }

    public function fileExportMark2()
    {
        return Excel::download(new MarkExport2, 'Mark2.xlsx');
    }
}
