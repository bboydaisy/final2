<?php

namespace App\Http\Controllers;
use App\Models\Classrm;
use App\Models\Mark1;
use App\Models\Mark2;
use App\Models\Student;
use App\Models\Subjects;
use Illuminate\Support\Facades\Redirect;
use App\Exports\MarkExport;
use App\Models\Condition;
use App\Models\Learnag;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;

class MarkAgController1 extends Controller
{
    public function index()
    {
        $classrms = Classrm::with('course')->orderBy('course_id', 'desc')->paginate(20)->withQueryString();
        return view('markag1.index', compact('classrms'));
    }

    public function show($id)
    {
        $classrm = Classrm::find($id);
        $students = Student::where('class_id', $id)->orderBy('name', 'ASC')->get();
        $subjectss = Subjects::orderBy('name', 'ASC')->get();
        $mark1s = Learnag::with('student', 'subjects')->orderBy('id', 'desc')->paginate(20)->withQueryString();
        return view('markag1.show', compact('mark1s', 'classrm', 'students', 'subjectss'));
    }

    public function edit($id)
    {
        $mark1 = Learnag::find($id);
        return view('markag1.edit', compact('mark1'));
    }

    public function update(Request $request)
    {
        $mark1 = Learnag::find($request->id);

        $mark1->mark1 = $request->mark1;
        $mark1->status = $request->status;
        $mark1->save();
        return Redirect::route('markag1-show', $mark1->class_id)->with('success', 'Cập nhật thành công!');
    }

    public function destroy(Mark1 $mark1)
    {
        //
    }

    public function fileExportMark()
    {
        return Excel::download(new MarkExport, 'Mark.xlsx');
    }
}
