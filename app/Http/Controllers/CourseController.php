<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{

    public function index()
    {
        $courses = Course::orderBy('id', 'desc')->paginate(20)->withQueryString();
        return view('course.index', compact('courses'));
    }


    public function store(Request $request)
    {
        Course::create($request->all());
        return redirect('/course')->with('success', 'Thêm khóa học thành công!');
    }


    public function show(Course $course)
    {
        //
    }


    public function edit($id)
    {
        $course = Course::find($id);
        return view('course.edit', compact('course'));
    }


    public function update(Request $request)
    {
        $course = Course::find($request->id);
        $course->update($request->all());
        return redirect('/course')->with('success', 'Cập nhật thành công!');
    }


    public function destroy(Course $course)
    {
        //
    }
}
