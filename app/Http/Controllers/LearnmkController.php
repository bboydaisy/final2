<?php

namespace App\Http\Controllers;

use App\Models\Condition;
use App\Models\Learnag;
use App\Models\Learnmk;
use Illuminate\Http\Request;

class LearnmkController extends Controller
{
    public function index($id)
    {
        $learnag = Learnag::find($id);
        $learnmks = Condition::where('learnag_id', $id)->first();
        return view('learnmk.index', compact('learnmks', 'learnag'));
    }

    public function store(Request $request)
    {
        $condition = Condition::where('learnag_id', $request->id)->first();
        $newCondition = new Condition();
        if ($request->point_ingredient < 5 || $request->status === 0) {
            $newCondition->mark1 = 0;
        }
        $newCondition->point_ingredient = $request->point_ingredient;
        $newCondition->student_id = $condition->student_id;
        $newCondition->subject_id = $condition->subject_id;
        $newCondition->class_id = $condition->class_id;
        $newCondition->status = $request->status;
        $newCondition->save();
        $learnag = Learnag::find($request->id);
        $learnag->status = 1;
        $learnag->save();
        return redirect()->route('condition-show', $newCondition->class_id)->with('success', 'Cập nhật thành công!');
    }
}
