<?php

namespace App\Http\Controllers;

use App\Models\Classrm;
use App\Models\Learnag;
use Illuminate\Http\Request;

class LearnagController extends Controller
{
    public function index()
    {
        $classrms = Classrm::with('course', 'learnag')->orderBy('course_id', 'desc')->paginate(20)->withQueryString();
        
        return view('learnag.index', compact('classrms'));
    }

    public function show($id)
    {
        $classrm = Classrm::find($id);
        $learnags = Learnag::where('class_id', $id)->where('status', '0')->orderBy('id', 'desc')->paginate(20)->withQueryString();
        return view('learnag.show', compact('learnags', 'classrm'));
    }
}
