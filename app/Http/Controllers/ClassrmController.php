<?php

namespace App\Http\Controllers;

use App\Models\Classrm;
use App\Models\Course;
use Illuminate\Http\Request;

class ClassrmController extends Controller
{

    public function index()
    {
        $classrms = Classrm::with('course')->orderBy('id', 'desc')->paginate(20)->withQueryString();
        $courses = Course::orderBy('name', 'ASC')->get();
        return view('class.index', compact('classrms', 'courses'));
    }


    public function store(Request $request)
    {
        Classrm::create($request->all());
        return redirect('/class')->with('success', 'Thêm lớp học thành công!');
    }


    public function show(Classrm $classrm)
    {
        //
    }


    public function edit($id)
    {
        $courses = Course::orderBy('name', 'ASC')->get();
        $classrm = Classrm::find($id);
        return view('class.edit', compact('classrm', 'courses'));
    }


    public function update(Request $request)
    {
        $classrm = Classrm::find($request->id);
        $classrm->update($request->all());
        return redirect('/class')->with('success', 'Cập nhật thành công!');
    }

    public function destroy(Classrm $classrm)
    {
        //
    }
}
