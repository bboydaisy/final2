<?php

namespace App\Http\Controllers;

use App\Models\Majors;
use App\Models\Subjects;
use Illuminate\Http\Request;

class SubjectsController extends Controller
{
    public function index()
    {
        $subjectss = Subjects::with('majors')->orderBy('id', 'desc')->paginate(20)->withQueryString();
        $majorss = Majors::orderBy('name', 'ASC')->get();
        return view('subjects.index', compact('subjectss', 'majorss'));
    }

    public function store(Request $request)
    {
        Subjects::create($request->all());
        return redirect('/subjects')->with('success', 'Thêm môn học thành công!');
    }

    public function show(Subjects $subjects)
    {
        //
    }

    public function edit($id)
    {
        $subjects = Subjects::find($id);
        $majorss = Majors::orderBy('name', 'ASC')->get();
        return view('subjects.edit', compact('subjects', 'majorss'));
    }

    public function update(Request $request)
    {
        $subjects = Subjects::find($request->id);
        $subjects->update($request->all());
        return redirect('/subjects')->with('success', 'Cập nhật thành công!');
    }

    public function destroy(Subjects $subjects)
    {
        //
    }
}
