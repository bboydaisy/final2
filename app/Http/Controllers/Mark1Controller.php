<?php

namespace App\Http\Controllers;

use App\Models\Classrm;
use App\Models\Mark1;
use App\Models\Mark2;
use App\Models\Student;
use App\Models\Subjects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Exports\MarkExport;
use App\Models\Condition;
use Maatwebsite\Excel\Facades\Excel;
// use App\Http\Requests\FRequest;

class Mark1Controller extends Controller
{

    public function index()
    {
        $classrms = Classrm::with('course')->orderBy('course_id', 'desc')->paginate(20)->withQueryString();
        return view('mark1.index', compact('classrms'));
    }

    public function show($id)
    {
        $classrm = Classrm::find($id);
        $students = Student::where('class_id', $id)->orderBy('name', 'ASC')->get();
        $subjectss = Subjects::orderBy('name', 'ASC')->get();
        $mark1s = Condition::with('student', 'subjects')->where('status', '1')->where([['class_id', $id],['mark1', '=', null]])->orWhere([['class_id', $id],['mark1', '>=', '5']])->orderBy('id', 'desc')->paginate(20)->withQueryString();
        return view('mark1.show', compact('mark1s', 'classrm', 'students', 'subjectss'));
    }

    public function edit($id)
    {
        $mark1 = Condition::find($id);
        return view('mark1.edit', compact('mark1'));
    }

    public function update(Request $request)
    {
        $mark1 = Condition::find($request->id);
        $mark1->mark1 = $request->mark1;
        $mark1->status = $request->status; 
        $mark1->save();
        return Redirect::route('mark1-show', $mark1->class_id)->with('success', 'Cập nhật thành công!');
    }

    public function fileExportMark()
    {
        return Excel::download(new MarkExport, 'Mark.xlsx');
    }
}
