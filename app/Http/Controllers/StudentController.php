<?php

namespace App\Http\Controllers;

use App\Models\Classrm;
use App\Models\Student;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use PDF;

class StudentController extends Controller
{

    public function index(Request $request)
    {
        $search = $request->input('search');
        if ($search) {
            $students = Student::with('classrm')
                ->where('name', 'like', "%$search%")
                ->orWhere('phone', 'like', "%$search%")
                ->paginate(20);
            $students->appends(['search' => $search]);
        } else {
            $students = Student::with('classrm')->orderBy('id', 'desc')->paginate(20);
        }
        $classrms = Classrm::orderBy('name', 'ASC')->get();
        return view('student.index', compact('students', 'classrms'));
    }



    public function store(Request $request)
    {
        Student::create($request->all());
        return redirect('/student')->with('success', 'Thêm sinh viên thành công!');
    }


    public function show(Student $student)
    {
        //
    }


    public function edit($id)
    {
        $student = Student::find($id);
        $classrms = Classrm::orderBy('name', 'ASC')->get();
        return view('student.edit', compact('student', 'classrms'));
    }


    public function update(Request $request)
    {
        $student = Student::find($request->id);
        $student->update($request->all());
        return redirect('/student')->with('success', 'Cập nhật thành công!');
    }


    public function destroy(Student $student)
    {
        //
    }


    public function fileExport()
    {
        return Excel::download(new UsersExport, 'student.xlsx');
    }


    public function fileImport(Request $request)
    {
        Excel::import(new UsersImport, $request->file('file')->store('temp'));
        return back();
    }
}
