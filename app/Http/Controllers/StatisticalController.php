<?php

namespace App\Http\Controllers;

use App\Models\Classrm;
use App\Models\Condition;
use App\Models\Course;
use App\Models\Learnag;
use App\Models\Learnmk;
use App\Models\Majors;
use App\Models\Mark1;
use App\Models\Student;
use App\Models\Subjects;
use App\Models\Teacher;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatisticalController extends Controller
{
    public function index()
    {
        $date = Carbon::now();
        $courses = Course::get()->count();
        $classrms = Classrm::get()->count();
        $students = Student::get()->count();
        $majorss = Majors::get()->count();
        $subjectss = Subjects::get()->count();
        $teachers = Teacher::get()->count();
        // $total_exam = Mark1::where('status', 0)->get()->count();
        $total_exam = Condition::where('mark1', '<', '5')->where('mark2', '=', null)->count();
        $total_learn = Learnag::where('status', '0')->get()->count();
        return view('index', compact(
            'date',
            'courses',
            'classrms',
            'students',
            'majorss',
            'subjectss',
            'teachers',
            'total_exam',
            'total_learn'
        ));
    }



}
