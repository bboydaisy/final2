<?php

namespace App\Http\Controllers;

use App\Exports\ConditionExport;
use App\Models\Classrm;
use App\Models\Condition;
use App\Models\Learnag;
use App\Models\Mark1;
use App\Models\Student;
use App\Models\Subjects;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\FRequest;

class ConditionController extends Controller
{
    public function index()
    {
        $classrms = Classrm::with('course')->orderBy('course_id', 'desc')->paginate(20)->withQueryString();
        return view('condition.index', compact('classrms'));
    }

    public function show($id)
    {
        $classrm = Classrm::find($id);
        $students = Student::where('class_id', $id)->orderBy('name', 'ASC')->get();
        $subjectss = Subjects::orderBy('name', 'ASC')->get();
        $conditions = Condition::with('student', 'subjects')->where('class_id', $id)->orderBy('id', 'desc')->paginate(20)->withQueryString();
        return view('condition.show', compact('conditions', 'classrm', 'students', 'subjectss'));
    }


    public function store(FRequest $request)
    {
        $condition = new Condition();
        if ($request->point_ingredient < 5 || $request->status === 0) {
            $condition->mark1 = 0;
        }
        $condition->point_ingredient = $request->point_ingredient;
        $condition->student_id = $request->student_id;
        $condition->subject_id = $request->subject_id;
        $condition->class_id = $request->id;
        $condition->status = $request->status;
        $condition->save();
        return redirect()->back()->with('success', 'Thêm điểm thành công!');
    }


    public function fileExportcondition()
    {
        return Excel::download(new ConditionExport, 'markcondition.xlsx');
    }

}
