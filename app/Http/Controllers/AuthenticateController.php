<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthenticateController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function signin(LoginRequest $request)
    {
        if (Auth::guard('teacher')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('/');
        }
        return redirect('/login')->with('error', 'Tài khoản hoặc mật khẩu không đúng!');
    }

    public function logout()
    {
        Auth::guard('teacher')->logout();
        return redirect('/login')->with('success', 'Đăng xuất thành công!');
    }
}
