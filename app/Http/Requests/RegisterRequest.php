<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:25',
            'email' => 'required|email|max:255|unique:teachers',
            'password' => 'required|min:8',
            'phone' => 'required|min:10',
            'confirm_flag' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => "Tên không được để trống!",
            'name.min' => "Tên có độ dài không được nhỏ hơn 2 ký tự!",
            'name.max' => "Tên có độ dài không được lớn hơn 25 ký tự!",
            'email.required' => "Email không được để trống!",
            'email.email' => "Email không hợp lệ!",
            'email.unique' => "Email đã tồn tại!",
            'password.required' => "Mật khẩu không được để trống!",
            'password.min' => "Mật khẩu tối thiểu 8 ký tự!",
            'phone.required' => "Số điện thoại không được để trống!",
            'name.min' => "Số điện thoại tối thiểu 8 ký tự!",
            'confirm_flag.required' => "Cấp độ không được để trống!",
        ];
    }
}
