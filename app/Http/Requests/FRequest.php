<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id' => 'required|',
            'subject_id' => 'required|',
            'class_id' => 'required',
            'status' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'student_id.required' => "Tên sinh viên không được để trống!",
            'subject_id.required' => "Tên môn học không được để trống!",
            'class_id.required' => "Tên lớp học không được để trống!",
            'status.required' => "Điều kiện không được để trống!",
        ];
    }
}
