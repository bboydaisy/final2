<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    use HasFactory;

    protected $fillable = [
        'point_ingredient',
        'student_id',
        'subject_id',
        'class_id',
        'status',
        'mark1',
        'mark2',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }

    public function subjects()
    {
        return $this->belongsTo(Subjects::class, 'subject_id', 'id');
    }
}
