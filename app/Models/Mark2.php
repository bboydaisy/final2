<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mark2 extends Model
{
    use HasFactory;

    protected $fillable = [
        'point',
        'student_id',
        'subject_id',
        'confirm_flag',
        'status',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }

    public function subjects()
    {
        return $this->belongsTo(Subjects::class, 'subject_id', 'id');
    }
}
