<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Majors extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    // public function subjects()
    // {
    //     return $this->hasMany(Subjects::class, 'subject', 'id');
    // }
}
