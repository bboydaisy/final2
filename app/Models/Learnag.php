<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Learnag extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'subject_id',
        'status',
        'mark1',
        'mark2',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }

    public function subjects()
    {
        return $this->belongsTo(Subjects::class, 'subject_id', 'id');
    }

    public function learnmk()
    {
        return $this->hasMany(Learnmk::class, 'learnag_id', 'id');
    }
}
