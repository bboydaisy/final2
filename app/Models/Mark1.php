<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mark1 extends Model
{
    use HasFactory;

    protected $fillable = [
        'point',
        'student_id',
        'subject_id',
        'confirm_flag',
        'status',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }

    public function subjects()
    {
        return $this->belongsTo(Subjects::class, 'subject_id', 'id');
    }

    // public function getTrangThaiAttribute(): string
    // {
    //     if ($this->status == 1) {
    //         return "Đạt";
    //     }

    //     return "Thi lại";
    // }

    // public function getDieuKienAttribute(): string
    // {
    //     if ($this->confirm_flag == 1) {
    //         return "Đủ";
    //     }

    //     return "Chưa";
    // }
}
