<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->userDefault();
    }

    public function userDefault()
    {
        \App\Models\Teacher::insert([
            [
                'name' => "Duongvq",
                'email' => "admin@bkacad.com",
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'phone' => '0523611885',
                'confirm_flag' => "1",
                'created_at' => now(),
            ],
            [
                'name' => "TungND",
                'email' => 'admin2@bkacad.com',
                'password' => '$2y$10$BpCvKqjlLLwK5s0FnLLNWeU0N2lxP3sI3esvYtKn/5FyhoT/Sw2lO',
                'phone' => '0904339998',
                'confirm_flag' => "2",
                'created_at' => now(), 
            ]
        ]);
    }
}
