<?php

use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\BrowserController;
use App\Http\Controllers\ClassrmController;
use App\Http\Controllers\ConditionController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\LearnagController;
use App\Http\Controllers\LearnmkController;
use App\Http\Controllers\MajorsController;
use App\Http\Controllers\Mark1Controller;
use App\Http\Controllers\Mark2Controller;
use App\Http\Controllers\MarkAgController1;
use App\Http\Controllers\MarkAgController2;
use App\Http\Controllers\StatisticalController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectsController;
use App\Http\Controllers\TeacherController;
use App\Http\Middleware\CheckLogin;
use App\Http\Middleware\CheckLogout;

use Illuminate\Support\Facades\Route;

Route::middleware([CheckLogin::class])->group(function () {
    // welcome
    Route::get('/', [StatisticalController::class, 'index']);
    // course
    Route::get('/course', [CourseController::class, 'index']);
    Route::post('/course', [CourseController::class, 'store']);
    Route::get('/course/{id}', [CourseController::class, 'edit'])->name('course-edit');
    Route::put('/course', [CourseController::class, 'update']);
    // class
    Route::get('/class', [ClassrmController::class, 'index']);
    Route::post('/class', [ClassrmController::class, 'store']);
    Route::get('/class-edit/{id}', [ClassrmController::class, 'edit'])->name('class-edit');
    Route::put('/class', [ClassrmController::class, 'update']);
    // student
    Route::get('/student', [StudentController::class, 'index']);
    Route::post('/student', [StudentController::class, 'store']);
    Route::get('/student-edit/{id}', [StudentController::class, 'edit'])->name('student-edit');
    Route::put('/student', [StudentController::class, 'update']);
    // majors
    Route::get('/majors', [MajorsController::class, 'index']);
    Route::post('/majors', [MajorsController::class, 'store']);
    Route::get('/majors-edit/{id}', [MajorsController::class, 'edit'])->name('majors-edit');
    Route::put('/majors', [MajorsController::class, 'update']);
    // subjects
    Route::get('/subjects', [SubjectsController::class, 'index']);
    Route::post('/subjects', [SubjectsController::class, 'store']);
    Route::get('/subjects-edit/{id}', [SubjectsController::class, 'edit'])->name('subjects-edit');
    Route::put('/subjects', [SubjectsController::class, 'update']);
    // condition
    Route::get('/condition', [ConditionController::class, 'index']);
    Route::get('/condition-show/{id}', [ConditionController::class, 'show'])->name('condition-show');
    Route::post('/condition', [ConditionController::class, 'store']);
    // mark1
    Route::get('/mark1', [Mark1Controller::class, 'index']);
    Route::get('/mark1-show/{id}', [Mark1Controller::class, 'show'])->name('mark1-show');
    Route::post('/mark1', [Mark1Controller::class, 'store']);
    Route::get('/mark1-edit/{id}', [Mark1Controller::class, 'edit'])->name('mark1-edit');
    Route::put('/mark1', [Mark1Controller::class, 'update']);
    // mark2
    Route::get('/mark2', [Mark2Controller::class, 'index']);
    Route::get('/mark2-show/{id}', [Mark2Controller::class, 'show'])->name('mark2-show');
    Route::get('/mark2-edit/{id}', [Mark2Controller::class, 'edit'])->name('mark2-edit');
    Route::put('/mark2', [Mark2Controller::class, 'update']);
    // markag1
    Route::get('/markag1', [MarkAgController1::class, 'index']);
    Route::get('/markag1-show/{id}', [MarkAgController1::class, 'show'])->name('markag1-show');
    Route::get('/markag1-edit/{id}', [MarkAgController1::class, 'edit'])->name('markag1-edit');
    Route::put('/markag1', [MarkAgController1::class, 'update']);
    // markag2
    Route::get('/markag2', [MarkAgController2::class, 'index']);
    Route::get('/markag2-show/{id}', [MarkAgController2::class, 'show'])->name('markag2-show');
    Route::get('/markag2-edit/{id}', [MarkAgController2::class, 'edit'])->name('markag2-edit');
    Route::put('/markag2', [MarkAgController2::class, 'update']);
    // teacher
    Route::get('/teacher', [TeacherController::class, 'index']);
    Route::get('/teacher-create', [TeacherController::class, 'create']);
    Route::post('/teacher', [TeacherController::class, 'store']);
    Route::get('/teacher-edit/{id}', [TeacherController::class, 'edit'])->name('teacher-edit');
    Route::put('/teacher', [TeacherController::class, 'update']);
    Route::delete('/teacher', [TeacherController::class, 'destroy']);
    //Export - Import excel
    Route::post('file-import', [StudentController::class, 'fileImport'])->name('file-import');
    Route::get('file-export', [StudentController::class, 'fileExport'])->name('file-export');
    Route::get('file-export-mark', [Mark1Controller::class, 'fileExportMark'])->name('file-export-mark');
    Route::get('file-export-mark2', [Mark2Controller::class, 'fileExportMark2'])->name('file-export-mark2');
    Route::get('file-export-conditon', [ConditionController::class, 'fileExportcondition'])->name('file-export-conditon');
    //chart
    Route::get('chart', [BrowserController::class, 'index']);
    Route::get('/chart-show/{id}', [BrowserController::class, 'show'])->name('chart-show');
    Route::get('/statistical', [BrowserController::class, 'statistical'])->name('statistical');
    // logout
    Route::get('/logout', [AuthenticateController::class, 'logout']);
});

Route::middleware([CheckLogout::class])->group(function () {
    // login
    Route::get('/login', [AuthenticateController::class, 'login']);
    Route::post('/login', [AuthenticateController::class, 'signin']);
});
